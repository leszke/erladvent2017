%%%-------------------------------------------------------------------
%%% @author FMX487
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. gru 2017 18:12
%%%-------------------------------------------------------------------
-module(zad18).
-author("FMX487").

%% API
-export([raz/0]).

raz() ->
  Map = maps:new(),
  [First | _] = Instructions = string:split(input(), "\r\n", all),
  compute(Instructions, string:split(lists:nth(1, Instructions), " ", all), 1, Map).

compute(Instructions, _, Ptr, Map) when Ptr > erlang:length(Instructions) ->
  Map;
compute(Instructions, [Op, Reg | [Val]], Ptr, Map) ->
  io:format("doing ~p ~p ~p~n", [Op, Reg, Val]),
  io:format("current mp: ~p~n", [Map]),
  TmpMap = case maps:find(Reg, Map) of
             {ok, _Value} -> Map;
             error -> maps:put(Reg, 0, Map)
           end,
  {NewMap, NewPtr} = case Op of
                       "set" -> case maps:find(Val, TmpMap) of
                                 error -> {maps:update(Reg, list_to_integer(Val), TmpMap), Ptr+1};
                                  {ok, Value} -> {maps:update(Reg, Value, TmpMap), Ptr+1}
                                end;
                         %{maps:put(Reg, erlang:list_to_integer(Val), TmpMap), Ptr+1};
                       "add" -> OldVal = maps:get(Reg, TmpMap),
                                case maps:find(Val, Map) of
                                  error -> {maps:update(Reg, OldVal + list_to_integer(Val), Map), Ptr+1};
                                  {ok, Value} -> {maps:update(Reg, OldVal+Value, TmpMap),Ptr+1}
                                end;
                       "mul" -> OldVal = maps:get(Reg, TmpMap),
                                 case maps:find(Val, Map) of
                                   error -> {maps:put(Reg, OldVal * list_to_integer(Val), Map), Ptr+1};
                                   {ok, Value} -> {maps:update(Reg, OldVal*Value, TmpMap),Ptr+1}
                                 end;
                       "mod" -> OldVal = maps:get(Reg, TmpMap),
                         case maps:find(Val, Map) of
                           error -> {maps:update(Reg, OldVal rem list_to_integer(Val), Map), Ptr+1};
                           {ok, Value} -> {maps:update(Reg, OldVal rem Value, TmpMap),Ptr+1}
                         end;
                        "jgz" -> case maps:get(Reg, TmpMap) > 0 of
                                   false -> {TmpMap, Ptr+1};
                                   true  -> {TmpMap, Ptr+list_to_integer(Val)}
                                 end
  end,
  NextInstr = string:split(lists:nth(NewPtr, Instructions), " ", all),
  compute(Instructions, NextInstr, NewPtr, NewMap);

compute(Instructions, [Op, Reg], Ptr, Map) ->
  io:format("doing ~p ~p~n", [Op, Reg]),
  io:format("current mp: ~p~n", [Map]),
  TmpMap = case maps:find(Reg, Map) of
             {ok, _Value} -> Map;
             error -> maps:put(Reg, 0, Map)
           end,
  case Op of
    "snd" -> {ok, Played} = maps:find(Reg, TmpMap),
              {NewMap, NewPtr} = {maps:put(played, Played, TmpMap), Ptr+1},
               NextInstr = string:split(lists:nth(NewPtr, Instructions), " ", all),
               compute(Instructions, NextInstr, NewPtr, NewMap);
    "rcv" -> case maps:get(Reg, TmpMap) > 0 of
               false -> {NewMap, NewPtr} = {TmpMap, Ptr+1},
                         NextInstr = string:split(lists:nth(NewPtr, Instructions), " ", all),
                          compute(Instructions, NextInstr, NewPtr, NewMap);
               true -> {maps:update(Reg, maps:get(played, TmpMap), TmpMap),Ptr+1},
                 maps:get(played, TmpMap)
             end
  end.

test_input() ->
"set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2".

input() ->
  "set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 680
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19".