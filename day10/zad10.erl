-module(zad10).
-export([raz/0,dwa/0, dwa/2]).

raz() ->
	List = lists:seq(0,255),
	Lengths = lists:map(fun(X) -> list_to_integer(X) end, string:split(raw_input(), ",", all)),
	{[H,H2|_T] =_List, _, _} = raz(List, Lengths, _Position = 0, _SkipSize = 0),
	H*H2.

raz(List, [] = _Lengths, Pos, SS) ->
	{List, Pos, SS};

raz(List, [CurrentLength|T] = _Lengths, Pos, SS) ->
	{BeforePosList, AfterPosList} = lists:split(Pos, List),
	NewList = case Pos + CurrentLength > length(List) of
	%%%% zlap <list[pos]; list[pos+currentlength]>
				false ->
					{ToReverse, Tail} = lists:split(CurrentLength, AfterPosList),
					BeforePosList ++ lists:reverse(ToReverse) ++ Tail;
				true ->
					RemCnt = (Pos + CurrentLength) rem erlang:length(List),
					{FrontToBeReversed, OriginalMiddle} = lists:split(RemCnt, BeforePosList),
					ToSplit = lists:reverse(AfterPosList ++ FrontToBeReversed),
					{NewBack, NewFront} = lists:split(erlang:length(ToSplit)-RemCnt, ToSplit),
					NewFront++OriginalMiddle++NewBack
	end,
	NewPos = (CurrentLength + Pos + SS) rem erlang:length(List),
	raz(NewList, T, NewPos, SS+1).

dwa() ->
	dwa(lists:seq(0,255), raw_input() ++ [17, 31, 73, 47, 23]).

dwa(List, Lengths) ->
	SparseHash = dwa(List, Lengths, _Pos = 0, _SS = 0, _Cntr = 64),
	ToBxor = [lists:sublist(SparseHash, X, 16) || X <- lists:seq(1,length(SparseHash), 16)],
	DenseHash = lists:foldr(fun(X, Acc) -> [compute_bxor(X)|Acc] end, [], ToBxor),
	to_hex(DenseHash).
	
dwa(List, _, _, _, 0) ->
	List;

dwa(List, Lengths, Pos, SS, Cntr) ->
	{NewList, NewPos, NewSS} = raz(List, Lengths, Pos, SS),
	dwa(NewList, Lengths, NewPos, NewSS, Cntr-1).

compute_bxor(List) ->
	lists:foldl(fun(Y, Acc) -> Acc bxor Y end, 0, List).

to_hex(Hash) -> to_hex(Hash, "").

to_hex([], Acc) -> Acc;
to_hex([H|T], Acc) ->
	Hex = erlang:integer_to_list(H,16),
	Res = case length(Hex) of
			1 -> "0"++Hex;
			2 -> Hex
	end,
	to_hex(T, Acc++Res).
test_input() ->
	"3,4,1,5".

raw_input() ->
	"230,1,2,221,97,252,168,169,57,99,0,254,181,255,235,167".	