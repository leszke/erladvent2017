-module(zad1).
-export([raz/1, dwa/1]).
%-compile(debug_info).

raz(Input) -> 
	Tmp = lists:map(fun(X) -> X-$0 end, Input),
	Tmp2 = Tmp ++ string:slice(Tmp,0,1),
	io:format("dbg: Lista: ~p~n", [Tmp2]),
	raz(Tmp2, 0, 0).

raz([X,Y], Acc, Res) when X == Y ->
	Res+Acc+X;
raz([X,Y], Acc, Res) when X /= Y ->
	Res+Acc;
raz([X,Y|T], Acc, Res) when X == Y ->
	raz([Y|T], Acc+X, Res);
raz([X,Y|T], Acc, Res) when X /= Y ->
	raz([Y|T], 0, Res+Acc).
	
dwa(Input) -> 
	L = lists:map(fun(X) -> X-$0 end, Input),
	io:format("dbg: Lista: ~p~n", [L]),
	{L1, L2} = lists:split(length(L) div 2, L),
	io:format("dbg: L1: ~p~nL2: ~p~n", [L1,L2]),
	dwa(L1, L2, 0).

dwa([],[], Res) -> Res;
dwa([H1|T1], [H2|T2], Res) when H1 == H2 ->
	dwa(T1,T2, Res+H1+H2);	
dwa([_H1|T1], [_H2|T2], Res) ->
	dwa(T1,T2, Res).