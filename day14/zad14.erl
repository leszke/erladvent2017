-module(zad14).
-compile(export_all).
-import(zad10, [raz/0,dwa/2]).

day14a() ->
	HashInputs = generate_inputs(),
	KnotHashes = lists:foldr(fun(X, Acc) -> [zad10:dwa(lists:seq(0,255), X)|Acc] end, [], HashInputs),
	WholeSpace = lists:foldr(fun(X, Acc) -> [hash_to_binary(X) | Acc] end, [], KnotHashes),
	lists:foldl(fun(X,Acc) -> Acc+count1s(X) end, 0, WholeSpace).

knot_hash(String) ->
		zad10:dwa(lists:seq(0,255), String++[17, 31, 73, 47, 23]).
	
generate_inputs() ->
	lists:map(fun(X) -> input()++"-"++integer_to_list(X)++[17, 31, 73, 47, 23] end, lists:seq(0,127)).

hash_to_binary(Hash) ->
	lists:foldr(fun(X, Acc) -> hex_to_binary(X) ++ Acc end, "", Hash).

count1s(List) -> 
	length([X || X<- List, X == $1]).

input() ->
	"hxtvlmkl".
	
%% xD dont want to mess with that
hex_to_binary(Number) ->
	case Number of
		$0 -> "0000";
		$1 -> "0001";
		$2 -> "0010";
		$3 -> "0011";
		$4 -> "0100";
		$5 -> "0101";
		$6 -> "0110";
		$7 -> "0111";
		$8 -> "1000";
		$9 -> "1001";
		$A -> "1010";
		$B -> "1011";
		$C -> "1100";
		$D -> "1101";
		$E -> "1110";
		$F -> "1111"
	end.