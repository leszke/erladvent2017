-module(zad13).
-export([day13a/0, day13b/0]).

day13a() ->
	Caught = [{X,Y} || {X,Y} <- gentle_input(), X rem (2*(Y-1)) == 0],
	lists:foldl(fun({X,Y}, Acc) -> X*Y+Acc end, 0, Caught).

day13b() ->
	find_delay(gentle_input(), 1).
	
make_int_tuple([Key, Value]) ->
	{erlang:list_to_integer(Key), erlang:list_to_integer(Value)}.

gentle_input() ->
	Scanners = string:split(raw_input(), [$\r,$\n], all),
	ScannersDepths = lists:foldr(fun(X, Acc) -> [string:split(X, ": ", all)|Acc] end ,[], Scanners),
	lists:foldr(fun(X, Acc) -> [make_int_tuple(X) | Acc] end, [], ScannersDepths).

find_delay(List, Delay) ->
	case length([{X,Y} || {X,Y} <- List, (X+Delay) rem (2*(Y-1)) == 0]) of
		0 -> Delay;
		_Moah -> find_delay(List, Delay+1)
	end.

test_input() ->
	"0: 3
1: 2
4: 4
6: 4".

raw_input() ->
	"0: 4
1: 2
2: 3
4: 5
6: 8
8: 4
10: 6
12: 6
14: 6
16: 10
18: 6
20: 12
22: 8
24: 9
26: 8
28: 8
30: 8
32: 12
34: 12
36: 12
38: 8
40: 10
42: 14
44: 12
46: 14
48: 12
50: 12
52: 12
54: 14
56: 14
58: 14
60: 12
62: 14
64: 14
68: 12
70: 14
74: 14
76: 14
78: 14
80: 17
82: 28
84: 18
86: 14".