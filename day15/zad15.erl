-module(zad15).
-compile(export_all).

raz() ->
	{A,B} = input(),
	raz(A,B,0,40000000).
	
raz(_,_,Acc, 0) ->
	Acc;
raz(A,B,Acc,Cntr) ->
	A2 = (A*16807) rem 2147483647,
	B2 = (B*48271) rem 2147483647,
	case A2 rem 65536 == B2 rem 65536 of
		true -> raz(A2,B2,Acc+1, Cntr-1);
		false ->raz(A2,B2,Acc, Cntr-1)
	end.

dwa() ->
	{A,B} = input(),
	dwa(A,B,0,5000000).

dwa(A,B,Acc,0) ->
	Acc;
dwa(A,B,Acc,Cntr) ->
	A2 = generator_a(A),
	B2 = generator_b(B),
	case A2 rem 65536 == B2 rem 65536 of
		true -> dwa(A2,B2,Acc+1, Cntr-1);
		false ->dwa(A2,B2,Acc, Cntr-1)
	end.

generator_a(Number) ->
	A = (Number*16807) rem 2147483647,
	case A rem 4 == 0 of
		true -> A;
		false -> generator_a(A)
	end.

generator_b(Number) ->
	B = (Number*48271) rem 2147483647,
	case B rem 8 == 0 of
		true -> B;
		false -> generator_b(B)
	end.

test_input() -> {65, 8921}.

input() -> {634, 301}.