-module(zad3).
-export([raz/0, dwa/0]).
-define(ETS, advent).
-define(THRESHOLD, 289326).

raz() ->
    Number = get_input(),
    X = find_largest_odd_natural(Number),
    Corner = X+1,
    Offset = ((Number-X*X) rem (X+1)),
    PathLength = case Offset > 0.5*Corner of
        true -> Offset;
        false -> Corner - Offset
    end,
    case X*X == Number of
        true -> X-1;
        false -> PathLength
    end.
   
find_largest_odd_natural(N) ->
    Tmp = [X || X <- lists:seq(1, erlang:trunc(math:sqrt(N)), 2), Y <- lists:seq(1,N), X*X==Y],
    lists:last(Tmp).
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dwa() ->
    ets:new(?ETS, [set, named_table]),
    insert(0,0,1),
    InitialX = 1,
    InitialY = 0,
    FirstDir = up,
    NextFieldNumber = 2,
    EdgeLength = find_edge_length(NextFieldNumber),
    compute_next_field(InitialX,InitialY,FirstDir, EdgeLength, NextFieldNumber, _LastInsertedValue = 1).
   
compute_next_field(_X,_Y, _D, _E, _F, LastInsertedValue) when LastInsertedValue > ?THRESHOLD ->
    io:format("~n~n~n..::CONGRATULATIONS::..~n~n~n",[]),
    exit_success;
compute_next_field(X,Y, Direction, 1, FieldNumber, _LastInsertedValue)->
    Val = scan_and_score(X,Y),
    insert(X,Y,Val),
    io:format("Iteration no: ~p inserted ~p into ~p;~p~n", [FieldNumber, Val,X,Y]),
    NewDir = change_dir(Direction),
    {NewX, NewY} = set_next_coordinates(X, Y, NewDir, FieldNumber),
    NewEdgeLength = find_edge_length(FieldNumber),
    compute_next_field(NewX,NewY, NewDir, NewEdgeLength, FieldNumber+1, Val);
compute_next_field(X,Y,Direction, EdgeLengthLeft, FieldNumber, _LastInsertedValue) ->
    Val = scan_and_score(X,Y),
    insert(X,Y,Val),
    io:format("Iteration no: ~p inserted ~p into ~p;~p~n", [FieldNumber, Val,X,Y]),
    {NewX, NewY} = set_next_coordinates(X, Y, Direction, FieldNumber),
    compute_next_field(NewX, NewY, Direction, EdgeLengthLeft-1, FieldNumber+1, Val).

insert(X,Y,N) ->
    ets:insert(?ETS, {{X,Y},N}).
   
scan_and_score(X,Y) ->
    lookup({X+1,Y})
    + lookup({X+1,Y+1})
    + lookup({X,Y+1})
    + lookup({X-1,Y+1})
    + lookup({X-1,Y})
    + lookup({X-1,Y-1})
    + lookup({X,Y-1})
    + lookup({X+1,Y-1}).
   
lookup({X,Y}) ->
    case ets:lookup(?ETS, {X,Y}) of
        [] -> 0;
        [{_Coord, Val}] -> Val
    end.

change_dir(Current) ->
    case Current of
        up -> left;
        left -> down;
        down -> right;
        right -> up;
        _ -> io:format("you really fucked sth up...~n",[])
    end.
   
set_next_coordinates(X,Y,Dir, FieldNumber) ->
    ActualDir = case FieldNumber rem 2 == 1 andalso math:sqrt(FieldNumber) == erlang:trunc(math:sqrt(FieldNumber)) of
                    true -> right;
                    false -> Dir
    end,
    case ActualDir of
        up -> {X,Y+1};
        left -> {X-1, Y};
        down -> {X, Y-1};
        right -> {X+1, Y}
    end.
   
find_edge_length(FieldNo) ->
    find_largest_odd_natural(FieldNo) + 1.

get_input() ->
    289326.