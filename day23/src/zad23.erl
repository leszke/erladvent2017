%%%-------------------------------------------------------------------
%%% @author FMX487
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. gru 2017 22:53
%%%-------------------------------------------------------------------
-module(zad23).
-author("FMX487").

%% API
-export([raz/0, dwa/0]).

raz() ->
  Map = maps:new(),
  [First | _] = Instructions = string:split(input(), "\r\n", all),
  compute(Instructions, 1, 0, Map).

compute(Instructions, Ptr, Muls, Map) when Ptr > erlang:length(Instructions) ->
  Map;
compute(Instructions,  Ptr, Muls, Map) ->
  [Op, Reg | [Val]] = string:split(lists:nth(Ptr, Instructions), " ", all),
  %io:format("doing ~p ~p ~p~n", [Op, Reg, Val]),
  %io:format("current mp: ~p~n", [Map]),
  TmpMap = case {maps:find(Reg, Map), Reg == "1"} of
             {{ok, _Value}, false} -> Map;
             {error, false} -> maps:put(Reg, 0, Map);
             {_, true} -> Map
           end,
  {NewMap, NewPtr, MulsDid} = case Op of
                       "set" -> case maps:find(Val, TmpMap) of
                                  error -> {maps:update(Reg, list_to_integer(Val), TmpMap), Ptr+1,0};
                                  {ok, Value} -> {maps:update(Reg, Value, TmpMap), Ptr+1,0}
                                end;
                       %{maps:put(Reg, erlang:list_to_integer(Val), TmpMap), Ptr+1};
                       "sub" -> OldVal = maps:get(Reg, TmpMap),
                         case maps:find(Val, Map) of
                           error -> {maps:update(Reg, OldVal - list_to_integer(Val), Map), Ptr+1,0};
                           {ok, Value} -> {maps:update(Reg, OldVal-Value, TmpMap),Ptr+1,0}
                         end;
                       "mul" -> OldVal = maps:get(Reg, TmpMap),
                         case maps:find(Val, Map) of
                           error -> {maps:put(Reg, OldVal * list_to_integer(Val), Map), Ptr+1,1};
                           {ok, Value} -> {maps:update(Reg, OldVal*Value, TmpMap),Ptr+1,1}
                         end;
                       "jnz" -> case maps:find(Reg, TmpMap) of
                                  error -> {TmpMap, Ptr+list_to_integer(Val), 0};
                                  {ok, V}  -> case V /= 0 of
                                                  true -> {TmpMap, Ptr+list_to_integer(Val),0};
                                                    false -> {TmpMap, Ptr+1,0}
                                                end
                                end
                     end,
  compute(Instructions, NewPtr, Muls+MulsDid, NewMap).

dwa() ->
  Map = maps:new(),
  StartMap = maps:put("a", 1, Map),
  [First | _] = Instructions = string:split(input(), "\r\n", all),
  compute(Instructions, 1, 0, StartMap).

test_input() ->
  "set a 1
  add a 2
  mul a a
  mod a 5
  snd a
  set a 0
  rcv a
  jgz a -1
  set a 1
  jgz a -2".

input() ->
  "set b 67
set c b
jnz a 2
jnz 1 5
mul b 100
sub b -100000
set c b
sub c -17000
set f 1
set d 2
set e 2
set g d
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8
sub d -1
set g d
sub g b
jnz g -13
jnz f 2
sub h -1
set g b
sub g c
jnz g 2
jnz 1 3
sub b -17
jnz 1 -23".
