-module(zad6).
-export([raz/0, dwa/0]).

raz() ->
	InitMap = maps:new(),
	Map = init_map(get_input(), 1, InitMap),

	raz(Map, 1, [maps:to_list(Map)]).
	% to nam da max wartosc z mapy: lists:max(maps:values(Mapa).
raz(Map, IterationNo, Acc) ->
	io:format("iteration no:~p~n", [IterationNo]),
	MaxValue = lists:max(maps:values(Map)),
	Key = find_first_occurence(MaxValue, Map),
	Map1 = maps:update(Key, 0, Map),
	NewMap = increment(Key+1, MaxValue, Map1, maps:size(Map1)),
	case lists:member(maps:to_list(NewMap), Acc) of
		true -> io:format("~n~n~n..::CONGRATULATIONS::..~n~n~n~p", [IterationNo]),
				IterationNo;
		false -> raz(NewMap, IterationNo+1, [maps:to_list(NewMap)|Acc])
	end.

dwa() ->
	InitMap = maps:new(),
	Map = init_map(get_input(), 1, InitMap),
	AccMap = maps:new(),
	dwa(Map, 1, maps:put(1, Map, AccMap)).
dwa(Map, IterationNo, Acc) ->
	io:format("iteration no:~p~n", [IterationNo]),
	MaxValue = lists:max(maps:values(Map)),
	Key = find_first_occurence(MaxValue, Map),
	Map1 = maps:update(Key, 0, Map),
	NewMap = increment(Key+1, MaxValue, Map1, maps:size(Map1)),
	case find_first_occurence(NewMap, Acc) of
		[] -> dwa(NewMap, IterationNo+1, maps:put(IterationNo, NewMap, Acc));
		Anything -> io:format("~n~n~n..::CONGRATULATIONS::..~n~n~n~p~n", [Anything]),
				IterationNo-Anything
	end.
	
init_map([], _Cnt, M) -> M;
init_map([H|T] = InputValues, Cnt, Map) ->
	NewMap = maps:put(Cnt, H, Map),
	init_map(T, Cnt+1, NewMap).

increment(_Index, 0, M, _Size) -> M;
increment(Index, Cnt, M, Size) when Index > Size ->
	increment(1, Cnt, M, Size);
increment(Index, Cnt, M, Size) ->
	NewMap = maps:update_with(Index, fun(X) -> X+1 end, M), % wsadz mu +1
	increment(Index+1, Cnt-1, NewMap, Size).
find_first_occurence(Val, Map) ->
	case maps:keys(maps:filter(fun(K,V) -> V == Val end, Map)) of
	[] -> [];
	[FirstKey|_] -> FirstKey
	end.
test_input() -> [0,2,7,0].
get_input() ->
	[5,	1,	10,	0,	1,	7,	13,	14,	3,	12,	8,	10,	7,	12,	0,	6].
