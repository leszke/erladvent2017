-module(zad17).
-compile(export_all).

raz() ->
	raz([0], _CurrPos = 0, input(), _Cntr = 1).

raz(List, CurrPos, Jump, 100000) ->
	lists:sublist(List, CurrPos+2, 1);
raz(List, CurrPos, Jump, Cntr) ->
	Cut = (CurrPos+Jump) rem length(List),
	{Before, After} = lists:split(Cut+1, List),
	%io:format("~p~n", [Before++[Cntr]++After]),
	raz(Before++[Cntr]++After, Cut+1, Jump, Cntr+1).

test_input() -> 3.
input() -> 367.